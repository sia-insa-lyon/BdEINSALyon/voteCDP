Vote campagne CDP :
Salut {{prenom|lower|capfirst }} !
La Campagne CdP {{ year }} touche à sa fin ce soir, et tu peux désormais voter pour tes listes préférées !
Comment ça fonctionne ?
Trie les listes par ordre de préférence, plus une liste est haut classée, plus elle gagne des points. La première liste première liste gagne 4 points, la deuxième gagne 3 points et ainsi de suite. Les 4 listes classées dernière ne remporte aucune point. À l'issue des votes, les 4 listes avec le plus de points constitueront la nouvelle équipe du Comité de Parrainage !
Les votes seront ouverts du {{ open_date }} au {{ close_date }}.

{{url}}