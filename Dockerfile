FROM python:3.8
EXPOSE 8000

# Install locales package and generate fr_FR.UTF-8 locale
RUN apt-get update && \
    apt-get install -y locales && \
    sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales

ENV LANG fr_FR.UTF-8
ENV LC_ALL fr_FR.UTF-8

WORKDIR /app

COPY requirements.txt /app
RUN pip3 install -r requirements.txt
COPY . /app
ENV DATABASE_URL postgres://cdp@db/cdp
ENV SECRET_KEY ''
ENV DJANGO_ENV 'prod'
ENV SEND_EMAIL '0'

ENV FROM_EMAIL ''
ENV RETURN_LINK ''
ENV MAILJET_API_KEY ''
ENV MAILJET_SECRET_KEY ''
ENV OPENING="2 15 15:00:00 2019"
ENV CLOSING="2 22 23:59:00 2019"
RUN chmod +x bash/run-prod.sh
CMD bash/run-prod.sh